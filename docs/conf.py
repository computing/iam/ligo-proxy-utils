# Copyright (C) 2024-present Cardiff University

import re
import sys

from ligo_proxy_utils import __version__ as lpu_version

SOURCE_URL = "https://git.ligo.org/computing/iam/ligo-proxy-utils"

# parse version number to get git reference
if match := re.search(r"\+g(\w+)(?:\Z|\.)", lpu_version):
    SOURCE_REF, = match.groups()
else:
    SOURCE_REF = lpu_version

# -- configuration

# project info
project = "ligo-proxy-utils"
copyright = "2015-present, Cardiff University"
author = "Duncan Macleod"

release = lpu_version

# basic options
default_role = "obj"

# -- theming

html_theme = "furo"
html_theme_options = {
    "source_edit_link": f"{SOURCE_URL}/-/edit/{SOURCE_REF}/docs/{{filename}}",
    "source_view_link": f"{SOURCE_URL}/-/blob/{SOURCE_REF}/docs/{{filename}}",
}

# -- extensions

extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinx_copybutton",
    "sphinx_inline_tabs",
    "sphinxarg.ext",
]

intersphinx_mapping = {
    "python": (
        f"https://docs.python.org/{sys.version_info.major}",
        None,
    ),
}
