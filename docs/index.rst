.. toctree:
    :hidden:

    LIGO Proxy Utils <self>

################
LIGO Proxy Utils
################

.. image:: https://badge.fury.io/py/ligo-proxy-utils.svg
   :target: https://badge.fury.io/py/ligo-proxy-utils
   :alt: ligo-proxy-utils PyPI release badge
.. image:: https://img.shields.io/pypi/l/ligo-proxy-utils.svg
   :target: https://choosealicense.com/licenses/gpl-3.0/
   :alt: ligo-proxy-utils license

``ligo_proxy_utils`` provides utilities for generating short-lived credentials
for LIGO.ORG identity holders.

============
Installation
============

The easiest way to install ``ligo-proxy-utils`` is with
`Conda <https://conda.io/>`__ or `Pip <https://pip.pypa.io/>`__:

.. tab:: Conda

    .. code-block:: shell
        :caption: Installing ligo-proxy-utils with conda

        conda install -c conda-forge ligo-proxy-utils

.. tab:: Pip

    .. code-block:: shell
        :caption: Installing ligo-proxy-utils with pip

        pip install ligo-proxy-utils

For more details including details for other platforms, see
:doc:`install`.

.. toctree::
    :hidden:

    install

=====
Usage
=====

The main user interface is provided by the `ligo-proxy-init` script.
Simply run:

.. code-block:: shell

    ligo-proxy-init albert.einstein@LIGO.ORG

replacing `albert.einstein` with your LIGO.ORG user name.
For full details see :doc:`ligo-proxy-init`.

.. toctree::
    :hidden:

    ligo-proxy-init

For more details on LIGO.ORG identities, please see

https://computing.docs.ligo.org/guide/account/
