.. _install:

###############################
Installing ``ligo-proxy-utils``
###############################

.. _conda:

=====
Conda
=====

The recommended method of installing ``ligo-proxy-utils`` is with
`Conda <https://conda.io>`__
(or `Mamba <https://mamba.readthedocs.io/>`__):

.. code-block:: bash
    :name: install-conda
    :caption: Installing ligo-proxy-utils with conda

    conda install -c conda-forge ligo-proxy-utils

.. _debian:

===============
Debian / Ubuntu
===============

.. code-block:: bash
    :name: install-debian
    :caption: Installing ligo-proxy-utils with Apt

    apt-get install ligo-proxy-utils

See the IGWN Computing Guide software repositories entry for
`Debian <https://computing.docs.ligo.org/guide/software/debian/>`__
for instructions on how to configure the required
IGWN Debian repositories.

.. _pip:

===
Pip
===

.. code-block:: bash
    :name: install-pip
    :caption: Installing ligo-proxy-utils with Pip

    python -m pip install ligo-proxy-utils

.. _el:

==================================================
RedHat Enterprise Linux / Alma Linux / Rocky Linux
==================================================

.. code-block:: bash
    :name: install-el
    :caption: Installing ligo-proxy-utils with DNF

    dnf install ligo-proxy-utils

See the IGWN Computing Guide software repositories entries for
`Rocky Linux 9 <https://computing.docs.ligo.org/guide/software/rl9/>`__
for instructions on how to configure the required IGWN repositories.
