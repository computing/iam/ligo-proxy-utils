# Releasing `ligo-proxy-utils`

The instructions below detail how to finalise a new release
for version `{X.Y.Z}`.

In all commands below that placeholder should be replaced
with an actual release version.

## 1. Update the packaging files

-   Create a new branch on which to update the OS packaging files:

    ```shell
    git checkout -b finalise-{X.Y.Z}
    ```

-   Bump the version in the Python package `ligo_proxy_utils/__init__.py` file

-   Bump versions and add changelog entries in OS packaging files:

    - `debian/changelog`
    - `rpm/python-ligo-proxy-utils.spec`

    and then commit the changes to the branch.

-   Push this branch to your fork:

    ```shell
    git push -u origin finalise-{X.Y.Z}
    ```

-   Open a merge request on GitLab to finalise the packaging update.

## 2. Tag the release

-   Draft release notes by looking through the merge requests associated
    with the relevant
    [milestone on GitLab](https://git.ligo.org/computing/iam/ligo-proxy-utils/-/milestones).

-   Create an annotated, signed tag in `git` using the release notes
    as the tag message:

    ```shell
    git tag --sign {X.Y.Z}
    ```

-   Push the tag to the project on GitLab:

    ```shell
    git push upstream {X.Y.Z}
    ```

These steps can also be completed using the GitLab UI by visiting

<https://git.ligo.org/computing/iam/ligo-proxy-utils/-/tags/new>

## 3. Create a Release on GitLab

-   Create a
    [Release on GitLab](https://git.ligo.org/computing/iam/ligo-proxy-utils/-/releases/new)
    copying the same release notes from the tag message.

    Make sure and correctly associate the correct Tag and Milestones to the Release.

## 4. Request new binary packages via the SCCB

-   To request Debian/RHEL packages to be built and distributed for this new
    version in the IGWN software repositories,
    [open an SCCB request](https://git.ligo.org/computing/sccb/-/issues/new).
